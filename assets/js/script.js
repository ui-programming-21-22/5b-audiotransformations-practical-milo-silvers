console.log("helooooo")

function pageLoaded()
{
    let canvas = document.getElementById("canvas");
    let context = canvas.getContext("2d");

    for (var index=0;index<1;index++)
    {
        for (var loop=0;loop<1;loop++)
        {
            context.save();
            context.strokeStyle = "#3662a8";
            context.translate(500+loop*100,250+index*100);
            drawSpiral(context,2*(loop+2)/(loop+1),-17*(index+7)/(index+1),30);
            context.restore();
        }
    }

    function drawSpiral(context,R,r,O)
    {
        var x1 = R-O;
        var y1 = 0;
        var index = 1;
        
        context.beginPath();
        context.moveTo(x1,y1);

        do
        {
            if (index>20000) break;
            var x2 = (R+r)*Math.cos(index*Math.PI/73) - (r+O)*Math.cos(((R+r)/r)*(index*Math.PI/72))
            var y2 = (R+r)*Math.sin(index*Math.PI/73) - (r+O)*Math.sin(((R+r)/r)*(index*Math.PI/70))
            context.lineTo(x2,y2);
            x1 = x2;
            y1 = y2;
            index++;
        } while (x2 != R-O && y2 != 0);
        context.stroke();
    }
}

setInterval(function()
{ 
    
    music.addEventListener('ended', function()
    {
        this.currentTime=0;
        this.play();
    
    },false)
    
}, 3000);

pageLoaded();